package com.topco.otmm.controller;

import java.security.SecureRandom;
import java.sql.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

//import net.guides.springboot2.springbootjerseyrestcrudjpa.exception.ResourceNotFoundException;
import com.topco.otmm.entity.ProductDetails;
import com.topco.otmm.repository.ProductDetailsRepository;
import com.topco.otmm.service.ProductDetailsService;

@Component
@Path("/api/v1")
public class ProductDetailsController {
	Logger logger = LoggerFactory.getLogger(ProductDetailsController.class);
	@Autowired
	private ProductDetailsRepository prodcutRepository;
	
	@GET
	@Produces("application/json")
	@Path("/getAllProducts")
	public Iterable<ProductDetails> getProducts() {
		return prodcutRepository.findAll();
	}

	@GET
	@Path(value = "/productTest")
	public String test() throws Exception {
		return "Success";
	}
	public String generateOrderId(int length) {
		StringBuilder randomString = new StringBuilder();
		final char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
		final SecureRandom random = new SecureRandom();
		for (int i = 0; i < length; i++) {
			randomString.append(chars[random.nextInt(chars.length)]);
		}

		return randomString.toString();
	}
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/addSupplyDetails")
	@PostMapping("/addSupplyDetails")
	public List<ProductDetails> addNewSupply(@RequestBody List<ProductDetails> product) {
		try {
			logger.info("Adding New Order Details to DB");
			//System.out.println(product.getTitle());
			String orderId="ORD"+generateOrderId(10);
			product.forEach(n -> {
				n.setOrderId(orderId);
			});
		        
		}catch(Exception e) {
			//logger.debug(e.printStackTrace());
		}
		
		return prodcutRepository.saveAll(product);
	}

	@GET
	@Produces("application/json")
	@Path("/product/{userID}")
	public ResponseEntity<List<ProductDetails>> getUserById(@PathParam(value = "userID") String userID)
			throws ResourceNotFoundException {
		logger.info("Getting the Order Details of userid: "+userID);
		List<ProductDetails> product = prodcutRepository.findByUserID(userID);
		return ResponseEntity.ok().body(product);
	}

	@PUT
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/product/{userID}/{upcCode}/orderID")
	public ResponseEntity<ProductDetails> updateSupplyDetails(@PathParam(value = "upcCode") String upcCode,
			@PathParam(value = "userID") String userID,@PathParam(value = "orderID") String orderId, @Valid @RequestBody ProductDetails supplyDetails)
			throws ResourceNotFoundException {
		try {
			logger.info("Updating the Order Details of userid: "+userID +" with upc code: "+upcCode+" and with orderid:"+orderId);
			ProductDetails product = prodcutRepository.findByUserIDAndUpcCodeAndOrderId(userID, upcCode,orderId);
			// DateTimeFormatter dtf = DateTimeFormatter.ofPattern("uuuu-MM-dd");
			// LocalDate updatedDate = LocalDate.now();
			Date updateDt = new Date(System.currentTimeMillis());
			product.setMimAmount(supplyDetails.getMimAmount());
			product.setQty(supplyDetails.getQty());
			product.setId(product.getId());
			product.setUpcCode(upcCode);
			product.setUserID(userID);
			product.setTitle(supplyDetails.getTitle());
			product.setShipFrom(supplyDetails.getShipFrom());
			product.setSuppliedDate(updateDt);
			product.setCurrencySymbol(supplyDetails.getCurrencySymbol());
			final ProductDetails updatedDetailsr = prodcutRepository.save(product);
			return ResponseEntity.ok().body(updatedDetailsr);
		} catch (Exception e) {

			logger.debug("savePrint", e.toString());
			logger.error(e.getMessage(), e);
			return null;
		}

	}


	@DELETE
	@Path("/product/{userID}/{upcCode}/orderID")
	public String deleteDetails(@PathParam(value = "upcCode") String upcCode,
			@PathParam(value = "userID") String userID,@PathParam(value = "orderID") String orderId) throws Exception {
		logger.info("Deleting the Order Details of userid: "+userID +" with upc code: "+upcCode+" and with orderid:"+orderId);
		ProductDetails product = prodcutRepository.findByUserIDAndUpcCodeAndOrderId(userID, upcCode,orderId);
		if (product != null) {
			prodcutRepository.delete(product);
			// Map<String, Boolean> response = new HashMap<>();

			return "Deleted";
		} else {
			return "No Details Deleted";
		}

	}
}
