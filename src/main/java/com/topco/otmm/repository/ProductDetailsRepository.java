package com.topco.otmm.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;

import com.topco.otmm.entity.ProductDetails;

public interface ProductDetailsRepository extends Repository<ProductDetails, Long>, JpaRepository<ProductDetails, Long> {

	List<ProductDetails> findByUserID(String userID);
	ProductDetails findByUserIDAndUpcCodeAndOrderId(String userID, String upcCode,String orderId);
	
}
