package com.topco.otmm.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "ProductDetails")
public class ProductDetails {

	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Long id;
	
    @NotNull
    @Column(name="orderID")
    private String orderId;
     
    @NotNull
    @Column(name = "title")
    private String title;
     
    @Column(name = "upcCode")
    private String upcCode;
     
    @Column(name = "shipFrom")
    private String shipFrom;
 
    @Column(name = "qty")
    private double qty;
     
    @Column(name = "supplier")
    private String supplier;
    
    @Column(name = "suppliedDate")
    private Date suppliedDate;
    
    @Column(name = "startDate")
    private Date startDate;
    
    @Column(name = "endDate")
    private Date endDate;

	@Column(name = "mimAmount")
    private double mimAmount;
     
    @Column(name = "userID")
    private String userID;
          
    @Column(name = "currencySymbol")
    private String currencySymbol;

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUpcCode() {
		return upcCode;
	}

	public void setUpcCode(String upcCode) {
		this.upcCode = upcCode;
	}
	
	 public Date getSuppliedDate() {
			return suppliedDate;
		}

		public void setSuppliedDate(Date suppliedDate) {
			this.suppliedDate = suppliedDate;
		}

	public String getShipFrom() {
		return shipFrom;
	}

	public void setShipFrom(String shipFrom) {
		this.shipFrom = shipFrom;
	}

	public double getQty() {
		return qty;
	}

	public void setQty(double qty) {
		this.qty = qty;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public double getMimAmount() {
		return mimAmount;
	}

	public void setMimAmount(double mimAmount) {
		this.mimAmount = mimAmount;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getCurrencySymbol() {
		return currencySymbol;
	}

	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "ProductDetails [id=" + id + ", orderId=" + orderId + ", title=" + title + ", upcCode=" + upcCode
				+ ", shipFrom=" + shipFrom + ", qty=" + qty + ", supplier=" + supplier + ", suppliedDate="
				+ suppliedDate + ", startDate=" + startDate + ", endDate=" + endDate + ", mimAmount=" + mimAmount
				+ ", userID=" + userID + ", currencySymbol=" + currencySymbol + "]";
	}

	public ProductDetails(Long id, @NotNull String orderId, @NotNull String title, String upcCode, String shipFrom,
			double qty, String supplier, Date suppliedDate, Date startDate, Date endDate, double mimAmount,
			String userID, String currencySymbol) {
		super();
		this.id = id;
		this.orderId = orderId;
		this.title = title;
		this.upcCode = upcCode;
		this.shipFrom = shipFrom;
		this.qty = qty;
		this.supplier = supplier;
		this.suppliedDate = suppliedDate;
		this.startDate = startDate;
		this.endDate = endDate;
		this.mimAmount = mimAmount;
		this.userID = userID;
		this.currencySymbol = currencySymbol;
	}

	public ProductDetails() {
		//super();
		// TODO Auto-generated constructor stub
	}
    
    
    
}
