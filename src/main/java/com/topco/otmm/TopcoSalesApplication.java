package com.topco.otmm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class TopcoSalesApplication extends SpringBootServletInitializer  {

	@Override  
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application)   
	{  
	return application.sources(TopcoSalesApplication.class);  
	}  
	public static void main(String[] args)   
	{  
	SpringApplication.run(TopcoSalesApplication.class, args);  
	}
	
}
