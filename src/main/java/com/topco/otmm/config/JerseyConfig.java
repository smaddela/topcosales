package com.topco.otmm.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import com.topco.otmm.controller.ProductDetailsController;

@Configuration
public class JerseyConfig extends ResourceConfig{
 
    public JerseyConfig() {
    	 //packages("com.topco.otmm.package");
      
        register(ProductDetailsController. class);
        
    }
}
