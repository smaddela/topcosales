package com.topco.otmm.service;

import java.security.SecureRandom;

public class ProductDetailsService {

	public String generateOrderId(int length) {
		StringBuilder randomString = new StringBuilder();
		final char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
		final SecureRandom random = new SecureRandom();
		for (int i = 0; i < length; i++) {
			randomString.append(chars[random.nextInt(chars.length)]);
		}

		return randomString.toString();
	}
}
